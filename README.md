# Fortumo Task #

## Requirements & Configurations ##

* Java 11
* Maven

## Run ##

* Can be build using `mvn clean package` will build ROOT.war
* Last build can be found in deploy/ROOT.war
* War will be deployed for default context path. Can be access `http://<ip>:<port>/`

### Number Add Request ###

It is assumed no param names are defined for the post request and value will be considered as param name as below.

Request: `curl -d 10 http://localhost:8080/`

### End Request ###

Request: `curl -d end http://localhost:8080/`