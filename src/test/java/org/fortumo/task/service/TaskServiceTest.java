package org.fortumo.task.service;

import org.junit.jupiter.api.Test;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TaskServiceTest
{
    @Test
    public void testDoPost_WhenNoParameterAvailable() throws Exception
    {
        HttpServletRequest request = mock( HttpServletRequest.class );
        HttpServletResponse response = mock( HttpServletResponse.class );
        AsyncContext context = mock( AsyncContext.class );

        when( request.startAsync( request, response ) ).thenReturn( context );
        when( context.getRequest() ).thenReturn( request );
        when( context.getResponse() ).thenReturn( response );
        when( request.getParameterMap() ).thenReturn( new HashMap<>() );

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter( stringWriter );
        when( response.getWriter() ).thenReturn( writer );

        TaskService taskService = new TaskService();
        taskService.doPost( request, response );

        writer.flush();
        assertTrue( stringWriter.toString().contains( "No request param is defined" ) );
    }

    @Test
    public void testDoPost_WhenInvalidParametersAvailable() throws Exception
    {
        HttpServletRequest request = mock( HttpServletRequest.class );
        HttpServletResponse response = mock( HttpServletResponse.class );
        AsyncContext context = mock( AsyncContext.class );

        when( request.startAsync( request, response ) ).thenReturn( context );
        when( context.getRequest() ).thenReturn( request );
        when( context.getResponse() ).thenReturn( response );
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put( "TST", new String[]{"TST"} );
        when( request.getParameterMap() ).thenReturn( parameters );

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter( stringWriter );
        when( response.getWriter() ).thenReturn( writer );

        TaskService taskService = new TaskService();
        taskService.doPost( request, response );

        writer.flush();
        assertTrue( stringWriter.toString().contains( "Invalid request first param: TST" ) );
    }

    @Test
    public void testDoPost_WhenEndSuccess() throws Exception
    {
        HttpServletRequest request = mock( HttpServletRequest.class );
        HttpServletResponse response = mock( HttpServletResponse.class );
        AsyncContext context = mock( AsyncContext.class );

        when( request.startAsync( request, response ) ).thenReturn( context );
        when( context.getRequest() ).thenReturn( request );
        when( context.getResponse() ).thenReturn( response );
        Map<String, String[]> parameters = new HashMap<>();
        parameters.put( "end", new String[]{"end"} );
        when( request.getParameterMap() ).thenReturn( parameters );

        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter( stringWriter );
        when( response.getWriter() ).thenReturn( writer );

        TaskService taskService = new TaskService();
        taskService.doPost( request, response );

        sleep( 5 );
        writer.flush();
        assertTrue( stringWriter.toString().contains( "0" ) );
    }

    @Test
    public void testDoPost_WhenNoAndEndSuccess() throws Exception
    {
        HttpServletRequest request1 = mock( HttpServletRequest.class );
        HttpServletResponse response1 = mock( HttpServletResponse.class );
        AsyncContext context1 = mock( AsyncContext.class );

        when( request1.startAsync( request1, response1 ) ).thenReturn( context1 );
        when( context1.getRequest() ).thenReturn( request1 );
        when( context1.getResponse() ).thenReturn( response1 );
        Map<String, String[]> parameters1 = new HashMap<>();
        parameters1.put( "505", new String[]{} );
        when( request1.getParameterMap() ).thenReturn( parameters1 );

        StringWriter stringWriter1 = new StringWriter();
        PrintWriter writer1 = new PrintWriter( stringWriter1 );
        when( response1.getWriter() ).thenReturn( writer1 );

        TaskService taskService = new TaskService();
        taskService.doPost( request1, response1 );

        HttpServletRequest request2 = mock( HttpServletRequest.class );
        HttpServletResponse response2 = mock( HttpServletResponse.class );
        AsyncContext context2 = mock( AsyncContext.class );

        when( request2.startAsync( request2, response2 ) ).thenReturn( context2 );
        when( context2.getRequest() ).thenReturn( request2 );
        when( context2.getResponse() ).thenReturn( response2 );
        Map<String, String[]> parameters2 = new HashMap<>();
        parameters2.put( "end", new String[]{} );
        when( request2.getParameterMap() ).thenReturn( parameters2 );

        StringWriter stringWriter2 = new StringWriter();
        PrintWriter writer2 = new PrintWriter( stringWriter2 );
        when( response2.getWriter() ).thenReturn( writer2 );

        taskService.doPost( request2, response2 );

        sleep( 5 );
        writer1.flush();
        writer2.flush();
        assertTrue( stringWriter1.toString().contains( "505" ) );
        assertTrue( stringWriter2.toString().contains( "505" ) );
    }

    private void sleep( int seconds )
    {
        try
        {
            TimeUnit.SECONDS.sleep( seconds );
        }
        catch ( InterruptedException e )
        {
            e.printStackTrace();
        }
    }
}