package org.fortumo.task.service.handler;

import org.fortumo.task.service.util.Pair;
import org.junit.jupiter.api.Test;

import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class RequestHandlerTest
{

    @Test
    void testAddRequest()
    {
        List<String> requests = new CopyOnWriteArrayList<>();
        var random = new Random();
        IntStream.range( 0, 1000 )
                .forEach( value -> requests.add( String.valueOf( Math.abs( random.nextInt( 10000000 ) ) ) ) );
        requests.set( 20, "end" );
        requests.set( 25, "end" );
        requests.set( 151, "end" );
        requests.set( 152, "end" );
        requests.set( 153, "end" );
        requests.set( 285, "end" );
        requests.set( 350, "end" );
        requests.set( 429, "end" );
        requests.set( 510, "end" );
        requests.set( 550, "end" );
        requests.set( 650, "end" );
        requests.set( 850, "end" );
        requests.set( 851, "end" );
        requests.set( 852, "end" );
        requests.set( 853, "end" );
        requests.set( 999, "end" );

        var service = Executors.newFixedThreadPool( 20 );
        var listRandom = new Random();
        List<Pair<AsyncContext, StringWriter>> contextOrder = Collections.synchronizedList( new ArrayList<>( 1001 ) );
        List<CompletableFuture<Void>> futures = new ArrayList<>( 1001 );

        for ( int i = 0; i < 20; i++ )
        {
            futures.add( CompletableFuture.runAsync( () -> process( requests, listRandom, contextOrder ), service ) );
        }

        CompletableFuture.allOf( futures.toArray( new CompletableFuture[]{} ) ).join();
        send( "end", contextOrder );

        long sum = -1;

        for ( Pair<AsyncContext, StringWriter> asyncContext : contextOrder )
        {
            HttpServletRequest request = ( HttpServletRequest ) asyncContext.getKey().getRequest();

            String requestVal = ( String ) request.getParameterMap().keySet().toArray()[0];
            long total = Long.parseLong( asyncContext.getValue().toString() );

            if ( "end".equals( requestVal ) )
            {
                assertEquals( sum, total );
                sum = -1;
            }
            else if ( sum == -1 )
            {
                sum = total;
            }
            else
            {
                assertEquals( sum, total );
            }
        }
    }

    private void process( List<String> requests, Random listRandom, List<Pair<AsyncContext, StringWriter>> contexts )
    {


        while ( !requests.isEmpty() )
        {
            try
            {
                String val = requests.remove( listRandom.nextInt( requests.size() ) );
                send( val, contexts );
            }
            catch ( Exception ex )
            {
                //ignored
            }
        }
    }

    private void send( String val, List<Pair<AsyncContext, StringWriter>> contexts )
    {
        try
        {
            var handler = RequestHandler.getInstance();

            HttpServletRequest request = mock( HttpServletRequest.class );
            HttpServletResponse response = mock( HttpServletResponse.class );
            AsyncContext context = mock( AsyncContext.class );

            when( request.startAsync( request, response ) ).thenReturn( context );
            when( context.getRequest() ).thenReturn( request );
            when( context.getResponse() ).thenReturn( response );
            Map<String, String[]> parameters = new HashMap<>();
            parameters.put( val, new String[]{} );
            when( request.getParameterMap() ).thenReturn( parameters );

            StringWriter stringWriter = new StringWriter();
            PrintWriter writer = new PrintWriter( stringWriter );
            when( response.getWriter() ).thenReturn( writer );
            System.out.println( context.hashCode() + " : " + val );
            contexts.add( new Pair<>( context, stringWriter ) );
            handler.addRequest( context );
        }
        catch ( IOException e )
        {
            e.printStackTrace();
        }
    }
}