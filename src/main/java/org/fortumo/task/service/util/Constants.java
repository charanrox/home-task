package org.fortumo.task.service.util;

public enum Constants
{
    REQUEST_TIMEOUT( 15 * 60 * 1000 );//In minutes

    private final long longValue;

    Constants( long longValue )
    {
        this.longValue = longValue;
    }

    public long getLongValue()
    {
        return longValue;
    }
}
