package org.fortumo.task.service.util;

public class Response<T>
{
    private final boolean error;
    private final String message;
    private final T data;

    public Response( boolean error, String message )
    {
        this( error, message, null );
    }

    public Response( boolean error, String message, T data )
    {
        this.error = error;
        this.message = message;
        this.data = data;
    }

    public boolean isError()
    {
        return error;
    }

    public String getMessage()
    {
        return message;
    }

    public T getData()
    {
        return data;
    }
}
