package org.fortumo.task.service.util;

import java.util.Objects;

public class Pair<C, O>
{
    private final C key;
    private final O value;

    public Pair( C key, O value )
    {
        this.key = key;
        this.value = value;
    }

    public C getKey()
    {
        return key;
    }

    public O getValue()
    {
        return value;
    }

    @Override
    public boolean equals( Object o )
    {
        if ( this == o )
            return true;
        if ( o == null || getClass() != o.getClass() )
            return false;
        Pair<?, ?> pair = ( Pair<?, ?> ) o;
        return Objects.equals( key, pair.key ) &&
                Objects.equals( value, pair.value );
    }

    @Override
    public int hashCode()
    {
        return Objects.hash( key, value );
    }
}
