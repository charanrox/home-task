package org.fortumo.task.service;

import org.fortumo.task.service.handler.RequestHandler;

import javax.servlet.AsyncContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.fortumo.task.service.util.Constants.REQUEST_TIMEOUT;

@WebServlet( urlPatterns = "/", asyncSupported = true )
public class TaskService extends HttpServlet
{
    @Override
    protected void doPost( HttpServletRequest req, HttpServletResponse resp )
    {
        final AsyncContext context = req.startAsync( req, resp );
        final RequestHandler requestHandler = RequestHandler.getInstance();
        context.setTimeout( REQUEST_TIMEOUT.getLongValue() );
        requestHandler.addRequest( context );
    }
}
