package org.fortumo.task.service.handler;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.fortumo.task.service.util.Pair;
import org.fortumo.task.service.util.Response;

import javax.servlet.AsyncContext;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;

public class RequestHandler
{
    private static final Logger LOGGER = LogManager.getLogger( RequestHandler.class );
    private final ExecutorService executorService;
    private final Queue<Pair<Long, AsyncContext>> requests;
    private final ReentrantLock lock = new ReentrantLock( true );

    private RequestHandler()
    {
        this( new ConcurrentLinkedQueue<>() );
    }

    RequestHandler( final Queue<Pair<Long, AsyncContext>> requests )
    {
        this.requests = requests;
        this.executorService = Executors.newFixedThreadPool( 20 );
    }

    public static RequestHandler getInstance()
    {
        return RequestHandlerHolder.REQUEST_HANDLER;
    }

    public void addRequest( AsyncContext asyncContext )
    {
        try
        {
            LOGGER.debug( "Request received: {}. Acquiring lock: {}, Existing: {}", asyncContext::hashCode, lock::isLocked, requests::size );
            lock.lock();
            LOGGER.debug( "Request Lock acquired: {}, Existing: {}", asyncContext::hashCode, requests::size );

            final ServletRequest request = asyncContext.getRequest();

            final Response<String> requestVal = getValue( request );

            if ( requestVal.isError() )
            {
                sendResponse( asyncContext, requestVal.getMessage() );
                LOGGER.error( "Request invalid: {}, Data: {}", asyncContext::hashCode, requestVal::getData );
                return;
            }

            if ( requestVal.getData().matches( "\\d+" ) )
            {
                long requestNo = Long.parseLong( requestVal.getData() );
                requests.add( new Pair<>( Long.parseLong( requestVal.getData() ), asyncContext ) );
                LOGGER.debug( "Number added: {}. Number: {} , Size: {}", asyncContext::hashCode, () -> requestNo, requests::size );
            }
            else
            {
                processWaitingQueue( asyncContext );
            }
        }
        catch ( Exception ex )
        {
            LOGGER.error( "Request failed. Req: " + asyncContext.hashCode(), ex );
        }
        finally
        {
            lock.unlock();
            LOGGER.debug( "Lock Released. Req: {}, Acquired: {}, Existing: {}",
                    asyncContext::hashCode, () -> true, requests::size );
        }
    }

    private void processWaitingQueue( AsyncContext asyncContext )
    {
        List<CompletableFuture<Void>> futures = new ArrayList<>( requests.size() + 1 );

        try
        {
            LOGGER.debug( "End received. Req: {}, Existing: {}", asyncContext::hashCode, requests::size );

            var total = requests.stream()
                    .mapToLong( Pair::getKey )
                    .sum();

            LOGGER.info( "--------------------------------------------------------------------------------------------------------" );
            LOGGER.info( "Started to process waiting requests: {}", asyncContext.hashCode() );
            LOGGER.info( "Queue: {}", requests.size() );
            LOGGER.info( "Sum: {}", total );
            LOGGER.info( "--------------------------------------------------------------------------------------------------------" );

            for ( Pair<Long, AsyncContext> pair : requests )
            {
                CompletableFuture<Void> future = CompletableFuture.runAsync( () -> sendResponse( pair, total ), executorService );
                futures.add( future );
            }

            futures.add( CompletableFuture.runAsync( () -> sendResponse( asyncContext, total ), executorService ) );
        }
        finally
        {
            requests.clear();
            LOGGER.info( "************************* Process restarted. Lock released: Req: {}, Size: {} *****************", asyncContext.hashCode(), requests.size() );
        }

        CompletableFuture.allOf( futures.toArray( new CompletableFuture[]{} ) )
                .thenAccept( unused -> LOGGER.info( "Request completed: {}", asyncContext.hashCode() ) );
    }

    private void sendResponse( AsyncContext context, long totalValue )
    {
        sendResponse( context, HttpServletResponse.SC_OK, String.valueOf( totalValue ) );
        LOGGER.debug( "Response sent for end. Req: {}, Total: {}",
                context::hashCode, () -> totalValue );
    }

    private void sendResponse( Pair<Long, AsyncContext> value, long totalValue )
    {
        AsyncContext asyncContext = value.getValue();
        sendResponse( asyncContext, HttpServletResponse.SC_OK, String.valueOf( totalValue ) );
        LOGGER.debug( "Response sent. Req: {}, Prev: {}, Total: {}",
                asyncContext::hashCode, value::getKey, () -> totalValue );
    }

    private void sendResponse( AsyncContext context, String data )
    {
        sendResponse( context, HttpServletResponse.SC_BAD_REQUEST, data );
    }

    private void sendResponse( AsyncContext context, int status, String data )
    {
        var servletResponse = ( HttpServletResponse ) context.getResponse();
        try ( PrintWriter out = servletResponse.getWriter() )
        {
            servletResponse.setStatus( status );
            out.print( data );
        }
        catch ( Exception e )
        {
            LOGGER.error( "Error in writing to response stream. Req: " + context.hashCode(), e );
            servletResponse.setStatus( SC_INTERNAL_SERVER_ERROR );
        }
        finally
        {
            context.complete();
        }
    }

    private Response<String> getValue( ServletRequest request )
    {
        if ( request.getParameterMap() == null || request.getParameterMap().isEmpty() )
        {
            return new Response<>( true, "No request param is defined" );
        }

        String firstParam = new ArrayList<>( request.getParameterMap().keySet() ).get( 0 );

        if ( !firstParam.matches( "\\d+" ) && !firstParam.equalsIgnoreCase( "end" ) )
        {
            return new Response<>( true, "Invalid request first param: " + firstParam );
        }

        return new Response<>( false, null, firstParam );
    }

    private static final class RequestHandlerHolder
    {
        private static final RequestHandler REQUEST_HANDLER = new RequestHandler();
    }
}
